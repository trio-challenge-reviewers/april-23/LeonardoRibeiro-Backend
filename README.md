# TRIO TAKE HOME PROJECT
## BACK END APP
* [SPECIFICATION](https://trio.notion.site/Back-End-Project-78fa9bd235be48fd82887f73055ae133)

## MAILCHIMP DATA
* MAILCHIMP_API_KEY: 0881cfd05349926ee84629066c43df78-us14
* MAILCHIMP_API_SERVER: us14
* MAILCHIMP_API_AUDIENCE_ID=409243c4d2

## Deliverables
* [TECHNICAL DESIGN](https://docs.google.com/document/d/1KXs8rq1CoF9ck8l-vnj7j7uhdw7y6pf4hAOgZ5TllWo/edit?usp=sharing)
* [INTEGRATION](https://triotakehome.herokuapp.com/contacts/sync/)

## Extra
* [NEWSLETTER SIGNUP](https://triotakehome.herokuapp.com/)
* [LIST MEMBERS](https://triotakehome.herokuapp.com/contacts/409243c4d2)

## Video
* [PRESENTATION](https://drive.google.com/file/d/1QNkEn0HbR-Yhrn7dYN-GudRA4Oo0NAHg/view?usp=sharing)
