const express = require('express');
const contactsRouter = require("./contacts");
const audiencesRouter = require("./audiences");
const router = express.Router();
router.use('/contacts', contactsRouter);
router.use('/audiences', audiencesRouter);
module.exports = router;
