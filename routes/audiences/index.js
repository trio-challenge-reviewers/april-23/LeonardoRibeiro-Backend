const express = require('express');
const router = express.Router();
const service = require('../../services/audiencesService');


router.delete('/:id', async (request, response) =>{
    console.log('delete audience..................');

    var { apiKey, apiServer } = request.body;
    
    if (apiKey === process.env.MAILCHIMP_API_KEY && apiServer == process.env.MAILCHIMP_API_SERVER) {
        service.audienceActions.deleteList(request.params.id)
            .then(data => {
                response.status(200);
                response.send({ message: "Audience "+request.params.id+ " were sucessfully deleted. "});
            }
            ).catch((error) => {
                if (error.status) {
                    response.status(error.status);
                    response.send(error);
                } else {
                    response.status(500);
                    response.send({ message: "An internal error happened in the server." });
                    console.log(error);
                }
            }
            );
       
    } else {
        response.status(401);
        response.send({ message: "You are not authorized to perform this operation" });
    }
});



router.delete('/:id/clean', async (request, response) =>{
    console.log('Clean audience..................');

    var { apiKey, apiServer } = request.body;
    
    if (apiKey === process.env.MAILCHIMP_API_KEY && apiServer == process.env.MAILCHIMP_API_SERVER) {
        service.audienceActions.cleanList(request.params.id)
            .then(data => {
                response.status(200);
                response.send({ message: "Audience "+request.params.id+ " were sucessfully cleaned. "});
            }
            ).catch((error) => {
                console.log(error);
                if (error.status) {
                    response.status(error.status);
                    response.send(error);
                } else {
                    response.status(500);
                    response.send({ message: "An internal error happened in the server." });
                    console.log(error);
                }
            }
            );
       
    } else {
        response.status(401);
        response.send({ message: "You are not authorized to perform this operation" });
    }
});

router.post('/create', async (request, response) => {
    console.log('create audience..................');
    console.log(request.body);

    var { listName, ownerName, email, apiKey, apiServer } = request.body;
    if (apiKey === process.env.MAILCHIMP_API_KEY && apiServer == process.env.MAILCHIMP_API_SERVER) {
        
        
        var listDetails =
        {
            name: listName,
            permission_reminder: "permission_reminder",
            email_type_option: true,
            contact: {
                company: "TRIO",
                address1: "https://trio.notion.site/",
                city: "Orlando",
                country: "USA",
                state: "Florida",
                zip:"2343213"
            },
            campaign_defaults: {
                from_name: ownerName,
                from_email: email,
                subject: "Welcome to my newsletter",
                language: "Portuguese",
            },
        }

        service.audienceActions.createList(listDetails)
            .then(data => {
                response.status(201);
                response.send(data);
            }
            ).catch((error) => {
                if (error.status) {
                    response.status(error.status);
                    response.send(error);
                } else {
                    response.status(500);
                    response.send({ message: "An internal error happened in the server. " + error });
                    console.log(error);
                }
            }
            );


    } else {
        response.status(401);
        response.send({ message: "You are not authorized to perform this operation" });
    }

});


module.exports = router;
