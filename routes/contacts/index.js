const express = require('express');
const router = express.Router();
const contactService = require('../../services/contactsService');

router.get('/sync', async (request, response) => {
    console.log('Contacts Syncronization Started.......');

    contactService.contactsActions.syncronizeContacts()
        .then(data => {
            data.syncedContacts > 0 ? response.status(201) : response.status(200);
            response.send(data);
        }
        ).catch((error) => {

            defaultErrorHandler(error, response);
        }
        );
});


//- this is an extra endpoint to list all contacts
router.get('/:id', async (request, response) => {
    console.log('get all contacts..................' + request.params.id);

    var members = await contactService.contactsActions.getMembers(request.params.id)
        .then(data => {
            response.status(200);
            response.send(data);
        }
        ).catch((error) => {
            defaultErrorHandler(error, response);
        }
        );
});

router.get('/', async (request, response) => {
    console.log('get all contacts..................' + process.env.MAILCHIMP_API_AUDIENCE_ID);

    var members = await contactService.contactsActions.getMembers(process.env.MAILCHIMP_API_AUDIENCE_ID)
        .then(data => {
            response.status(200);
            response.send(data);
        }
        ).catch((error) => {
            defaultErrorHandler(error, response);
        }
        );
});

//- this is an extra endpoint to be used for and individual member subscription from a HTML form (index.html)
router.post('/signup', async (request, response) => {
    console.log('signup..................');

    var { firstName, lastName, email } = request.body;

    var member = {
        firstName: firstName,
        lastName: lastName,
        email: email
    }


    contactService.contactsActions.addOrEditMember(process.env.MAILCHIMP_API_AUDIENCE_ID, member)
        .then(data => {
            response.status(201);
            response.redirect('/success.html');
        }
        ).catch((error) => {
            console.log(error);
            if (error.status) {
                response.status(error.status);
                response.redirect('/fail.html');
            } else {
                response.status(500);
                response.redirect('/fail.html');
            }

        }
        );

});

function defaultErrorHandler(error, response) {
    console.log(error);

    if (error.status) {
        response.status(error.status);
        response.send(error);
    } else
        if (error.response && error.response.status) {
            response.status(error.response.status);
            response.send(error);
        } else {
            response.status(500);
            response.send({ message: "An internal error happened in the server. " + error });
        }
}


module.exports = router;



