const mockService = require('./external/mockService');
const mailChimpService = require('./external/mailChimpService');

async function syncronizeContacts() {
    try {

        var mockData = await mockService.loadMockData();
        var returnData = await mailChimpService.mailChimpApi.uploadMembers(process.env.MAILCHIMP_API_AUDIENCE_ID, mockData);
        return returnData;

    } catch (error) {
        throw error;
    }

}

async function getMember(audienceID, memberToGet) {
    try {
        var contact = await mailChimpService.mailChimpApi.getMember(audienceID, memberToGet);
        return contact;

    } catch (error) {
        throw error;
    }
}



async function getMembers(audienceID) {
    try {

        var contacts = await mailChimpService.mailChimpApi.getMembers(audienceID);
        return contacts;

    } catch (error) {
        throw error;
    }
}

async function addOrEditMember(audienceID, memberToUpload) {
    try {
        var uploadedMember = await mailChimpService.mailChimpApi
            .addOrEditMember(audienceID, memberToUpload)
            .catch(error=>{
                throw error;
            });
            

        return uploadedMember;

    } catch (e) {
        throw e;
    }

}

async function deleteMember(audienceID, memberToDelete) {
    try {
        var deletedMember = await mailChimpService.mailChimpApi.deleteMember(audienceID, memberToDelete);
        return deletedMember;

    } catch (error) {
        throw error;
    }

}

const contactsActions = {
    syncronizeContacts: syncronizeContacts,
    addOrEditMember: addOrEditMember,
    getMembers: getMembers,
    getMember: getMember,
    deleteMember:deleteMember
}
exports.contactsActions = contactsActions;
