const mailchimp = require("@mailchimp/mailchimp_marketing");
var crypto = require('crypto');


mailchimp.setConfig({
    apiKey: process.env.MAILCHIMP_API_KEY,
    server: process.env.MAILCHIMP_API_SERVER,
});



async function uploadMembers(audienceID, listOfContacts) {

    var returnValue = {
        syncedContacts: 0,
        contacts: [],
        issues: []
    }

    return new Promise(async (resolve, reject) => {

        for (var count = 0; count < listOfContacts.length; count++) {

            try {
                await addOrEditMember(audienceID, listOfContacts[count])
                    .then((data) => {
                        
                        returnValue.syncedContacts = returnValue.syncedContacts + 1;
                        returnValue.contacts.push(listOfContacts[count]);
                    });


            } catch (error) {

                //- application should not stop if an error occurs with some member
                if (error.response) {
                    if (error.response.body) {
                        var errorDetail = {
                            status: error.response.body.status,
                            detail: error.response.body.detail
                        }
                        returnValue.issues.push(errorDetail);
                    }
                   
                } else {
                    console.log(error);
                }

            }

        }

        resolve(returnValue);


    });

}

async function addOrEditMember(audienceID, newMemberToUpload) {

    return new Promise(async function (resolve, reject) {
        try {
            if (!newMemberToUpload.firstName || !newMemberToUpload.lastName || !newMemberToUpload.email) {
                reject({ message: "Firstname, LastName and Emails are mandatory fields", status: 403 });
            }
            
            var member = await editMember(audienceID, newMemberToUpload)
            resolve(member);
            
        } catch (error) {
            reject(error);

        }

    });

}

async function addMember(audienceID, newMemberToUpload ) {
    
    var member = await mailchimp.lists.addListMember(
        audienceID,
        {
            email_address: newMemberToUpload.email,
            merge_fields: {
                FNAME: newMemberToUpload.firstName,
                LNAME: newMemberToUpload.lastName
            },
            status: "subscribed"
        });

    return member;
}

async function editMember(audienceID, newMemberToUpload ) {
    var emailHash = getHashValue(newMemberToUpload.email);

    var member = await mailchimp.lists.setListMember(
        audienceID,
        emailHash,
        {
            email_address: newMemberToUpload.email,
            merge_fields: {
                FNAME: newMemberToUpload.firstName,
                LNAME: newMemberToUpload.lastName
            },
            status_if_new: "subscribed",
            status: "subscribed"
        });

    return member;
}

function getHashValue(email) {
    return crypto.createHash('md5')
        .update(email.toLowerCase())
        .digest('hex');
}

async function getMember(audienceID, memberToGet) {
    
    var emailHash = crypto.createHash('md5')
    .update(memberToGet.email.toLowerCase())
    .digest('hex')

    var member = await mailchimp.lists.getListMember(audienceID, emailHash);
    return member;

}

async function deleteMember(audienceID, memberToDelete) {
    
    var emailHash = crypto.createHash('md5')
    .update(memberToDelete.email.toLowerCase())
    .digest('hex')

   //var result = await mailchimp.lists.deleteListMemberPermanent(audienceID, emailHash);
    var result = await mailchimp.lists.deleteListMember(audienceID, emailHash);
    
    return result;

}



async function getMembers(audienceID) {
    var returnValue = {
        qty: 0,
        contacts: []
    }

    return new Promise(async function (resolve, reject) {
        try {
            

            const data = await mailchimp.lists.getListMembersInfo(audienceID, { count: process.env.MAX_MEMBERS_TO_SHOW });
            returnValue.qty = data.members.length;
            for (var count = 0; count < data.members.length; count++) {
                returnValue.contacts.push(
                    { 
                        firstName: data.members[count].merge_fields.FNAME, 
                        lastName: data.members[count].merge_fields.LNAME,
                        email: data.members[count].email_address
                    }
                );
            }

            resolve(returnValue);

        } catch (error) {
            reject(error);

        }

    });


}

async function deleteList(audienceID) {

    return new Promise(async function (resolve, reject) {
        try {

            const deleteResponse = await mailchimp.lists.deleteList(audienceID);
            resolve(deleteResponse);

        } catch (error) {
            reject(error);

        }

    });

}


async function createList(listDetails) {

    return new Promise(async function (resolve, reject) {
        try {

            const createResponse = await mailchimp.lists.createList(listDetails);
            resolve(createResponse);

        } catch (error) {
            reject(error);

        }

    });

}

const mailChimpApi = {
    uploadMembers: uploadMembers,
    addOrEditMember: addOrEditMember,
    deleteMember:deleteMember,
    getMember:getMember,
    getMembers: getMembers,
    createList: createList,
    deleteList: deleteList   
    
}

exports.mailChimpApi = mailChimpApi;


