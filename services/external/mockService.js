const axios = require('axios')

function getMockedEmail(email){
    return Math.floor(Math.random() * 10000)+"_"+email;
}

function loadMockData() {
    
    return new Promise(async function (resolve, reject) {
        try {
            var mockData = await axios.get(process.env.MOCK_API_SERVER);
            
            if(!mockData || !mockData.data || mockData.data.length==0){
                resolve(mockData.data);
            }
            var returningData = [];

            for (var count = 0; count < mockData.data.length; count++) {

                //- This is just to bypass the email validation during tests.
                if(process.env.PASS_BANNED_EMAIL==="true"){
                    mockData.data[count].email = getMockedEmail(mockData.data[count].email);
                }

                returningData.push({
                    firstName:mockData.data[count].firstName,
                    lastName:mockData.data[count].lastName,
                    email:mockData.data[count].email
                });
            }
            resolve(returningData);

        } catch (error) {
            reject(error);
        }

    });
}

exports.loadMockData = loadMockData;


