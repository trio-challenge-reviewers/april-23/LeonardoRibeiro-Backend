const mailChimpService = require('./external/mailChimpService');


async function deleteList(audienceID) {
    try {
        var deleteResponse = await mailChimpService.mailChimpApi.deleteList(audienceID);
        return deleteResponse;

    } catch (error) {
        throw error;
    }

}

async function cleanList(audienceID) {
    try {

        var list = await mailChimpService.mailChimpApi.getMembers(audienceID);
        for (var count = 0; count < list.contacts.length; count++) {
            await mailChimpService.mailChimpApi.deleteMember(audienceID, list.contacts[count] );
        }

    } catch (error) {
        throw error;
    }

}

async function createList(listDetails) {
    try {

        var newList = await mailChimpService.mailChimpApi.createList(listDetails);
        return newList;

    } catch (error) {
        throw error;
    }

}

const audienceActions = {
    createList: createList,
    deleteList: deleteList,
    cleanList: cleanList
}
exports.audienceActions = audienceActions;
