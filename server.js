
const server = require('./app')
const port = process.env.PORT || 4000;
server.app.listen(port, () => console.log("App listening on port "+ port));