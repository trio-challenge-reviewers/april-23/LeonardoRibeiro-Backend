const request = require('supertest')
const app = require('../app')
const mailchimp = require("@mailchimp/mailchimp_marketing");
const axios = require('axios')
var assert = require('assert');

const contactsService = require('../services/contactsService');
const audiencesService = require('../services/audiencesService');

const mockService = require('../services/external/mockService');
const { fail } = require('assert');

mailchimp.setConfig({
  apiKey: process.env.MAILCHIMP_API_KEY,
  server: process.env.MAILCHIMP_API_SERVER,
});

/*
Unit Tests: 
  MOCK API
    Verify is mock api is returning data
  
  Mailchimp - Members
    Try to add a member using an invalid audience id 
    Try to add a member using a valid audience id. Check if all fields were correctly persisted
    Try to add a member with missing fields should respond with 403
    Try to update a member and check if a field were correctly updated
    Try to delete a member
  
Integration Test: /contact/sync api 
  Check if all records were synced in the end of the processing
  Check if the syncedContacts value corresponds to the contacts size
  Check if all syncedContacts are in fact synchronized in mailchimp api. 

Integration Test: audiences
    Check if clean method is working fine

*/


describe("Unit Tests - Mock API ", () => {


  test("Verify is mock api is returning data", async () => {
    await mockService.loadMockData()
      .then((data) => {
        assert(data.length > 0);
      })
      .catch((error) => {
        fail();
      });

  });

});


describe("Unit Tests - Mailchimp Members", () => {

  test("Try to add a member using an invalid audience id", async () => {

    var member = {
      firstName: 'Leonardo',
      lastName: 'Ribeiro',
      email: 'anyemail@gmail.com'
    }

    await contactsService.contactsActions.addOrEditMember('invalidAudienceId', member)
      .then((data) => {
        fail();
      })
      .catch((error) => {
        expect(error.status).toBe(404);
      });

  });

  test("Try to add a member using a valid audience id. Check if all fields were correctly persisted", async () => {

    var member = {
      firstName: 'Leonardo',
      lastName: 'Ribeiro',
      email: 'anyemail2@gmail.com'
    }


    await contactsService.contactsActions
      .addOrEditMember(process.env.MAILCHIMP_API_AUDIENCE_ID, member)
      .then(async (data) => {

        var insertedMember = await contactsService.contactsActions.getMember(process.env.MAILCHIMP_API_AUDIENCE_ID, member);
        assert(insertedMember.merge_fields.FNAME === member.firstName && insertedMember.merge_fields.LNAME === member.lastName);

      }).catch(error => {

        assert(false);
      });

  });

  test("Try to add a member with missing fields should respond with 403", async () => {

    var member = {
      firstName: 'Leonardo',
      lastName: 'Ribeiro'
    }

    await contactsService.contactsActions
      .addOrEditMember(process.env.MAILCHIMP_API_AUDIENCE_ID, member)
      .then(async (data) => {
        fail();

      }).catch(error => {

        expect(error.status).toBe(403);
      });

  });

  test("Try to update a member and check if a field were correctly updated", async () => {

    var member = {
      firstName: 'Leonardo',
      lastName: 'Ribeiro',
      email: 'leoliveira82@gmail.com'
    }

    await contactsService.contactsActions
      .addOrEditMember(process.env.MAILCHIMP_API_AUDIENCE_ID, member)
      .catch(error => {
        fail();
      });

    var insertedMember = await contactsService.contactsActions.getMember(process.env.MAILCHIMP_API_AUDIENCE_ID, member);

    member.lastName = 'Oliveira';

    await contactsService.contactsActions
      .addOrEditMember(process.env.MAILCHIMP_API_AUDIENCE_ID, member)
      .then(data => {
        assert(insertedMember.merge_fields.LNAME !== data.merge_fields.LNAME);

      })
      .catch(error => {
        fail();
      });

  });


  test("Try to delete a member", async () => {

    var member = {
      firstName: 'Leonardo',
      lastName: 'Ribeiro',
      email: 'testeDeletion@gmail.com'
    }

    await contactsService.contactsActions
      .addOrEditMember(process.env.MAILCHIMP_API_AUDIENCE_ID, member)
      .catch(error => {
        assert(false);
      });

    try {
      var deletedMember = await contactsService.contactsActions
        .deleteMember(process.env.MAILCHIMP_API_AUDIENCE_ID, member);

      var insertedMember = await contactsService.contactsActions
        .getMember(process.env.MAILCHIMP_API_AUDIENCE_ID, member);

    } catch (error) {
      expect(error.status).toBe(405);
    }

  });


});

/*
Test the /contact/sync api 
    Check if all records were synced in the end of the processing. Check if the syncedContacts value corresponds to the contacts size
    Check if all syncedContacts are in fact synchronized in mailchimp api. Check if all 3 fields match.
*/
describe("Integration Test: /contact/sync api ", () => {

  test("Check if all records were synced in the end of the processing ", async () => {

    var mockData = await axios.get(process.env.MOCK_API_SERVER);
    const contactsResponse = await request(app.app).get("/contacts/sync").send()
      .then((data) => {

        assert(data.body.syncedContacts === data.body.contacts.length && mockData.data.length === data.body.contacts.length);
      }).catch((error) => {
        fail();
      });

  });

  test("Check if all syncedContacts are in fact synchronized in mailchimp api. Check if all 3 fields match", async () => {

    var mockData = await axios.get(process.env.MOCK_API_SERVER);
    const contactsResponse = await request(app.app).get("/contacts/sync").send()
      .then(async (data) => {

        var allSyncedMember = true;
        for (var count = 0; count < data.body.contacts.length; count++) {
          var member = data.body.contacts[count];
          var memberToCompare = await contactsService.contactsActions.getMember(process.env.MAILCHIMP_API_AUDIENCE_ID, member);

          if (member.firstName !== memberToCompare.merge_fields.FNAME
            || member.lastName !== memberToCompare.merge_fields.LNAME
          ) {

            allSyncedMember = false;
            break;
          }
        }

        assert(allSyncedMember);

      }).catch((error) => {
        console.log(error);
        assert(false);
      });
  });

})


describe("Integration Test: /audiences/:id/clean ", () => {

  test("Check if clean method is working fine", async () => {

    var member = {
      firstName: 'Leonardo',
      lastName: 'Ribeiro',
      email: 'leoliveira82@gmail.com'
    }

    await contactsService.contactsActions
      .addOrEditMember(process.env.MAILCHIMP_API_AUDIENCE_ID, member)
      .catch(error => {
        fail();
      });

    await request(app.app)
      .delete("/audiences/" + process.env.MAILCHIMP_API_AUDIENCE_ID + "/clean")
      .send({
        apiKey: process.env.MAILCHIMP_API_KEY, 
        apiServer: process.env.MAILCHIMP_API_SERVER
      })
      .then()
      .catch((error) => {
        fail();
      });

    var list = await contactsService.contactsActions
      .getMembers(process.env.MAILCHIMP_API_AUDIENCE_ID)
      .catch((error) => {
        fail();
      });
    expect(list.qty).toBe(0);

  });


})




async function getAudienceId() {

  var listsData = await mailchimp.lists.getAllLists();
  return listsData.lists[0].id;
}
